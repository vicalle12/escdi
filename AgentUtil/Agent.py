"""
.. module:: Agent

Agent
******

:Description: Agent
  Clase para guardar los atributos de un agente

"""

__author__ = 'bejar'

from AgentUtil.OntoNamespaces import ACL, DSO
from rdflib import Namespace, Graph, Literal
from rdflib.namespace import FOAF, RDF
from AgentUtil.OntoNamespaces import ACL, DSO
from AgentUtil.ACLMessages import build_message, send_message, get_message_properties

class Agent():
    def __init__(self, name, uri, address, stop):
        self.name = name
        self.uri = uri
        self.address = address
        self.stop = stop


# Configuration constants and variables
agn = Namespace("http://www.agentes.org#")

# Datos del agent Address
DirectoryAgent = Agent('DirectoryAgent',
                       agn.Directory,
                       'http://%s:%d/Register' % ("0.0.0.0", 8000),
                       'http://%s:%d/Stop' % ('0.0.0.0', 8000))

def directorySearchMessage(type, agent, host, dport):
    DirectoryAgent.address = 'http://%s:%d/Register' % (host, dport)

    gmess = Graph()
    gmess.bind('foaf', FOAF)
    gmess.bind('dso', DSO)
    reg_obj = agn[agent.name + '-search']
    gmess.add((reg_obj, RDF.type, DSO.Search))
    gmess.add((reg_obj, DSO.AgentType, type))

    msg = build_message(gmess, perf=ACL.request,
                        sender=agent.uri,
                        receiver=DirectoryAgent.uri,
                        content=reg_obj)
    gr = send_message(msg, DirectoryAgent.address)

    msg = gr.value(predicate=RDF.type, object=ACL.FipaAclMessage)
    content = gr.value(subject=msg, predicate=ACL.content)

    print("the content\n" + content)

    ragn_addr = gr.value(subject=content, predicate=DSO.Address)
    rang_uri = gr.value(subject=content, predicate=DSO.Uri)

    return str(ragn_addr)

def registerMessage(type, agent, host, dport):
    print("Registrando agente: " + agent.name)
    DirectoryAgent.address = 'http://%s:%d/Register' % (host, dport)

    gmess = Graph()
    # Construimos el mensaje de registro
    gmess.bind('foaf', FOAF)
    gmess.bind('dso', DSO)
    reg_obj = agn[agent.name + '-Register']
    gmess.add((reg_obj, RDF.type, DSO.Register))
    gmess.add((reg_obj, DSO.Uri, agent.uri))
    gmess.add((reg_obj, FOAF.Name, Literal(agent.name)))
    gmess.add((reg_obj, DSO.Address, Literal(agent.address)))
    gmess.add((reg_obj, DSO.AgentType, type))
    # Lo metemos en un envoltorio FIPA-ACL y lo enviamos
    gr = send_message(
        build_message(gmess, perf=ACL.request,
                      sender=agent.uri,
                      receiver=DirectoryAgent.uri,
                      content=reg_obj),
        DirectoryAgent.address)
    print("Agente registrado")

    return "registrat"

def getDirectoryAgentAddress():
        return DirectoryAgent.address
