# -*- coding: utf-8 -*-
"""
Esqueleto de agente usando los servicios web de Flask

/comm es la entrada para la recepcion de mensajes del agente
/Stop es la entrada que para el agente

Tiene una funcion AgentBehavior1 que se lanza como un thread concurrente

Asume que el agente de registro esta en el puerto 9000

@author: javier
"""


from multiprocessing import Process, Queue
import socket

from rdflib import Namespace, Graph, URIRef, Literal, BNode
from rdflib.namespace import FOAF, RDF


from flask import Flask, request, render_template, send_from_directory
import requests

import constantes


from AgentUtil.OntoNamespaces import ACL, DSO
from AgentUtil.FlaskServer import shutdown_server
from AgentUtil.Agent import Agent
from AgentUtil.ACLMessages import build_message, send_message, get_message_properties
from math import *


__author__ = 'javier'

# Configuration stuff
hostname = constantes.host
port = constantes.vendedorExternoPort



agn = Namespace("http://www.agentes.org#")
transportistasNs = Namespace("http://www.agentes.org/transportista#")
centrosLoginsticosNs = Namespace("http://www.agentes.org/centroLogistico#")
clientNs = Namespace("http://www.agentes.org/cliente#")
COMPRA = Namespace("http://www.agentes.org/compra#")

# Contador de mensajes
mss_cnt = 0

# Datos del Agente
vendedorExternoAgent = Agent('vendedorExternoAgent',
                       agn.vendedorExternoAgent,
                       'http://%s:%d/comm' % (hostname, constantes.centroLogisticoPort),
                       'http://%s:%d/Stop' % (hostname, constantes.centroLogisticoPort))

# Datos del Agente
tiendaAgent = Agent('TiendaAgent',
                       agn.TiendaAgent,
                       'http://%s:%d/comm' % (hostname, port),
                       'http://%s:%d/Stop' % (hostname, port))


# Global triplestore graph
dsgraph = Graph()

transportistasGraph = Graph()
transportistasGraph.parse('transportista/transportista.rdf')

centrosLoginsticosGraph = Graph()
centrosLoginsticosGraph.parse('centroLogistico/centrologistico.rdf')

cola1 = Queue()

# Flask stuff
app = Flask(__name__)

class LatLong(object):
    def __init__(self, lat, long):
        self.lat = lat
        self.long = long

@app.route("/comm")
def comunicacion():
    """
    Entrypoint de comunicacion
    """
    def printGraph(graphToPrint):
        print 'pinta graph'
        for s, p, o in graphToPrint:
            print '--------------------'
            print 's', s
            print 'p', p
            print 'o', o
        return

    global dsgraph
    global mss_cnt
    print 'message received en vendedor externo agent'

    userLat = ""
    userLong = ""
    maxDaysToDeliver = ""
    # Extraemos el mensaje y creamos un grafo con él
    message = request.args['content']
    gm = Graph()
    gm.parse(data=message)
    msgdic = get_message_properties(gm)

    printGraph(gm)
    print (gm.serialize(format="turtle"))

    # Comprobamos que sea un mensaje FIPA ACL
    if msgdic is None:
        # Si no es, respondemos que no hemos entendido el mensaje
        print ("No entendi el mensaje")
        gr = build_message(Graph(), ACL['not-understood'], sender=tiendaAgent.uri, msgcnt=mss_cnt)
    else:
        # Obtenemos la performativa
        print ("Entendi el mensaje")
        perf = msgdic['performative']

        if perf != ACL.request:
            # Si no es un request, respondemos que no hemos entendido el mensaje
            print "CASI ENTIENDO EL MENSAJE, PERO NO"
            gr = build_message(Graph(), ACL['not-understood'], sender=vendedorExternoAgent.uri, msgcnt=mss_cnt)
        else:
            # Extraemos el objeto del contenido que ha de ser una accion de la ontologia de acciones del agente
            # de registro

            # Averiguamos el tipo de la accion
            print "REALMENTE ENTENDI EL MENSAJE"
            if 'content' in msgdic:
                content = msgdic['content']
                accion = gm.value(subject=content, predicate=RDF.type)

                #if action = enviar compra

                getData = """
                        SELECT *
                        WHERE {
                            ?a client:lat ?lat .
                            ?a client:long ?long .
                            ?a ns2:maxDaysToDeliver ?maxDaysToDeliver .
                        }
                    """

                res = gm.query(getData, initNs=dict(client=clientNs,ns2=COMPRA))

                print "--------------------------Query result-----------------------"
                print res.result
                while len(res.result) == 0:
                    res = gm.query(getData)
                    print res.result

                for row in res:
                    print row.lat
                    lat = row.lat
                    print row.long
                    long = row.long
                    print row.maxDaysToDeliver
                    maxDaysToDeliver = row.maxDaysToDeliver

                # getTransportista
                tranportista = searchTransportista(maxDaysToDeliver)
                print "-------------------------Transportista elegido-----------------------------"

                print tranportista.name
                print tranportista.dias_entrega
                print tranportista.tarifa

                # getCentroLogistico
                centroLogistico = searchCentroLogisticoByLatLong(lat, long)
                print "------------------------Almacen vendedor externo elegido---------------------------"
                #print centroLogistico
                # for row in centroLogistico:
                #     print "NMAW ", row.name

                print "Name ", centroLogistico.name

                # Create graph to send
                gmess = Graph()
                # We make bindings
                gmess.bind('centrologitico', centrosLoginsticosNs)
                gmess.bind('transportista', transportistasNs)

                reg_obj = agn[vendedorExternoAgent.name + '.getInfoDeliver']

                # info to get the nearest centro logístico
                gmess.add((reg_obj, centrosLoginsticosNs.name, Literal(centroLogistico.name)))
                gmess.add((reg_obj, centrosLoginsticosNs.lat, Literal(centroLogistico.lat)))
                gmess.add((reg_obj, centrosLoginsticosNs.long, Literal(centroLogistico.long)))

                gmess.add((reg_obj, transportistasNs.name, Literal(tranportista.name)))
                gmess.add((reg_obj, transportistasNs.dias_entrega, Literal(tranportista.dias_entrega)))
                gmess.add((reg_obj, transportistasNs.tarifa, Literal(tranportista.tarifa)))

                gr = build_message(gmess,
                                   ACL['inform-done'],
                                   sender=vendedorExternoAgent.uri,
                                   msgcnt=mss_cnt,
                                   receiver=msgdic['sender'],
                                   content=reg_obj)


    mss_cnt += 1

    print "HOLA COMPI CASI ACABO"

    #retorna el missatge

    return gr.serialize(format='xml')

@app.route("/receive", methods=['POST'])
def receive():
    """
    Entrypoint de comunicacion
    """
    message = 'Message received en vendedor externo'
    print message
    try:
        message = getForm('message')
        print 'message ', message
        return "Message received"
    except Exception, e:
        print e
    return 'No message? ', message


@app.route("/Stop")
def stop():
    """
    Entrypoint que para el agente

    :return:
    """
    tidyup()
    shutdown_server()
    return "Parando Servidor"

@app.route("/")
def render_main():
    """
        Main page for centro logístico
    """
    return render_template('tienda.html', title='vendedor externo')

# @app.route("/getTransportista")
def searchTransportista(daysToDeliver):

    print "Days to deliver ", daysToDeliver

    # print transportistasGraph.serialize(format='turtle')

    maxDeliverDaysFilter = "FILTER(?dias_entrega <= %s) ." % daysToDeliver

    getTransportistas = """
        SELECT *
        WHERE {
            ?a transportista:name ?name .
            ?a transportista:dias_entrega ?dias_entrega .
            ?a transportista:tarifa ?tarifa .
            %s
        }
    """ % (maxDeliverDaysFilter)

    res = transportistasGraph.query(getTransportistas, initNs=dict(transportista=transportistasNs))
    print "--------------------------Query result-----------------------"
    print res.result

    tarifaMinima = 0
    selectedTransportista = None
    i = 0
    for row in res:
        if i == 0:
            tarifaMinima = row.tarifa
            selectedTransportista = row
        elif row.tarifa < tarifaMinima:
            tarifaMinima = row.tarifa
            selectedTransportista = row

    return selectedTransportista

#funcion para calcular distancias entre latitudes y longitudes
def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    km = 6367 * c
    return km

# @app.route("/getCentroLogistico")
def searchCentroLogisticoByLatLong(lat, long):

    userLatLong = LatLong(lat, long)

    # print centrosLoginsticosGraph.serialize(format='turtle')

    # retorna tots els centros logisticos
    getCentrosLogisticos = """
        SELECT *
        WHERE {
            ?a centro:name ?name .
            ?a centro:lat ?lat .
            ?a centro:long ?long .
        }
    """
    res = centrosLoginsticosGraph.query(getCentrosLogisticos, initNs=dict(centro=centrosLoginsticosNs))

    distanciasArray = []
    distanciaMinima = 0
    distanciaFinal = 0
    selectedCentroLogistico = None
    i = 0
    for row in res:
        distanciasArray.append(LatLong(row.lat, row.long))
        if i == 0:
            # initialize loop for i = 0
            distanciaMinima = haversine(float(row.lat), float(row.long),
                                        float(userLatLong.lat),
                                        float(userLatLong.long))
            distanciaFinal = LatLong(float(row.lat), float(row.long))
            selectedCentroLogistico = row
        else:
           distancia = haversine(float(row.lat), float(row.long), float(userLatLong.lat), float(userLatLong.long))
           if distancia < distanciaMinima:
               distanciaMinima = distancia
               distanciaFinal.lat = row.lat
               distanciaFinal.long = row.long
               selectedCentroLogistico = row
        i = i + 1


    return selectedCentroLogistico

def tidyup():
    """
    Acciones previas a parar el agente

    """
    pass


def agentbehavior1(cola):
    """
    Un comportamiento del agente

    :return:
    """
    pass


'''
Función para obtener las request del Flask y evitar Exceptions
'''
def getForm(name):
    print 'Request is ' , request
    try:
        result = request.values[name]
    except Exception, e:
        print 'Error ', e
        result = ""
    return result

def getFormList(name):
    result = []
    try:
        result = request.values.getlist(name)
    except Exception, e:
        result = ""
    return result


if __name__ == '__main__':
    print 'Start vendedor externo agent'

    # Ponemos en marcha los behaviors
    ab1 = Process(target=agentbehavior1, args=(cola1,))
    ab1.start()

    # Ponemos en marcha el servidor
    app.run(host=hostname, port=port)

    # Esperamos a que acaben los behaviors
    ab1.join()
    print 'The End'
