# -*- coding: utf-8 -*-
from multiprocessing import Process, Queue
import socket

import constantes

from decimal import *

from rdflib import Namespace, Graph, Literal, BNode
from rdflib.term import URIRef
from rdflib.namespace import RDF, RDFS, Namespace, FOAF, OWL

from AgentUtil.ACLMessages import build_message, send_message, get_message_properties

from AgentUtil.OntoNamespaces import ACL

from rdflib.plugins.sparql import prepareQuery

from AgentUtil.FlaskServer import shutdown_server
from AgentUtil.Agent import Agent

import uuid

from flask import Flask, request, render_template, send_from_directory, url_for
import requests
from random import randint

hostname = constantes.host
port = constantes.tiendaPort

AGN = Namespace("http://www.agentes.org#")
TR = Namespace("http://www.agentes.org/transportista#")
CLIENT = Namespace("http://www.agentes.org/cliente#")
PRODUCT = Namespace("http://www.agentes.org/product#")
COMPRA = Namespace("http://www.agentes.org/compra#")
CL = Namespace("http://www.agentes.org/centroLogistico#")



vendedorExternoAgent = Agent('centroLogisticoAgent',
                       AGN.vendedorExternoAgent,
                       'http://%s:%d/comm' % (hostname, constantes.vendedorExternoPort),
                       'http://%s:%d/Stop' % (hostname, constantes.vendedorExternoPort))

# Datos del Agente
centroLogisticoAgent = Agent('centroLogisticoAgent',
                       AGN.centroLogisticoAgent,
                       'http://%s:%d/comm' % (hostname, constantes.centroLogisticoPort),
                       'http://%s:%d/Stop' % (hostname, constantes.centroLogisticoPort))

# Datos del Agente
tiendaAgent = Agent('TiendaAgent',
                       AGN.TiendaAgent,
                       'http://%s:%d/comm' % (hostname, port),
                       'http://%s:%d/Stop' % (hostname, port))

cola1 = Queue()

# Flask stuff
app = Flask(__name__)

usersG = Graph()
usersG.parse('cliente/cliente.rdf')

mss_cnt = 0



'''
----------------------------- HELPER FUNCTIONS -----------------------------------
'''

def esPosibleDevolver():
    y = randint(1, 10)
    if y < 3:
        return False
    else:
        return True


'''
----------------------------- PRINTER FUNCTIONS -----------------------------------
'''
def saveGraphToFile(filename,format):
    try:
        gmess.serialize(filename,format=format,encoding="utf-8")
    except Exception, e:
        print e
    return

def printGraph(graphToPrint):
    for s, p, o in graphToPrint:
        print '--------------------'
        print 's', s
        print 'p', p
        print 'o', o
    return


def printPrefs(prefs):
    print 'name', prefs['name']
    print 'days', prefs['days']
    print 'productType', prefs['productType']
    print 'minPrice', prefs['minPrice']
    print 'maxPrice', prefs['maxPrice']
    print 'sellerType', prefs['sellerType']
    return



def deepOverGraph(grapthToPrint):
    print "\n Graph subjects."
    for s in grapthToPrint.subjects(RDF.type, PRODUCT['Product']):
        print '************************************************'
        print s
        print '************************************************'
        for p in grapthToPrint.predicates(s):
            print p
            print grapthToPrint.value(subject=s,predicate=p)
            print '-------------'
    return

def printErrorPage(title,code):
    if not code:
        code = 400
    try:
        return render_template('errorPage.html',title=title,code=code), code
    except Exception, e:
        print 'error', e

'''
----------------------------- QUERY FUNCTIONS -----------------------------------
'''

def findUser(username):
        resource = CLIENT[username]
        q = prepareQuery(
            """
            SELECT *
            WHERE {
                ?res Client:username ?username .
                ?res Client:name ?name .
                ?res Client:lat ?lat .
                ?res Client:long ?long .
                ?res Client:tarjeta_credito ?tarjeta_credito .
            }
            """ ,
            initNs = dict(Client=CLIENT)
        )

        res = usersG.query(q, initBindings = dict(res = resource))

        u = None
        for user in res:
            u = user
        return u

def findProducts(prefs):
    try:
        print '-------- Preferencias --------'
        printPrefs(prefs)

        g = Graph()
        g.parse('producto/product.rdf')

        dictObj ={}
        # if name is None name = '?name'
        if prefs['name']:
            dictObj['name'] = Literal(prefs['name'])
        else :
            name = "?name"

        if prefs['sellerType'] :
            dictObj['vendedor'] = Literal(prefs['sellerType'])
        else :
            sellerType = "?vendedor"

        if prefs['productType'] :
            dictObj['type'] = Literal(prefs['productType'])
        else :
            productType = "?type"

        if prefs['minPrice'] :
            minPriceFilter = "FILTER(?precio >= %s) ." % prefs['minPrice']
        else :
            minPriceFilter = ""

        if prefs['maxPrice'] :
            maxPriceFilter = "FILTER(?precio <= %s) ." % prefs['maxPrice']
        else :
            maxPriceFilter = ""
        print 'lets query'

        preparedWithFilters = """
        SELECT *
        WHERE {
            ?a PRODUCT:name ?name .
            ?a PRODUCT:precio ?precio .
            ?a PRODUCT:vendedor ?vendedor .
            ?a PRODUCT:type ?type .
            %s
            %s
        }
        """ % (minPriceFilter, maxPriceFilter)

        print 'RAW QUERY ', preparedWithFilters
        q = prepareQuery(
            preparedWithFilters,
            initNs = dict(PRODUCT=PRODUCT)
        )

        print 'Q IS ', q


        queryResult = g.query(q, initBindings= dictObj)

        print "--------------------------Query result-----------------------"
        print queryResult
        print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
        for i in queryResult:
            print "---------------------------- i --------------------------------"
            print i
        print "-------------------------------------------------------------"

        #print '-------- Grafo --------'
        #deepOverGraph(g)

        return queryResult
    except Exception, e:
        print 'exception ', e
        return ''


def delegateCentroLogistico(user,prefs,products):
    try:
        global mss_cnt

        print 'We are preparing the graph to send'
        #Create graph to send
        gmess = Graph()
        #We make bindings
        gmess.bind('client', CLIENT)
        gmess.bind('compra', COMPRA)


        reg_obj =  AGN[centroLogisticoAgent.name + '.getInfoSale']

        print 'Reg_obj is ' , reg_obj

        #info to get the nearest centro logístico
        gmess.add((reg_obj,  CLIENT.lat, Literal(user.lat)))
        gmess.add((reg_obj,  CLIENT.long, Literal(user.long)))
        gmess.add((reg_obj,  COMPRA.maxDaysToDeliver, Literal(prefs['days'])))



        print 'We are goint to add the products '

        for p in products:
            print 'P NAME IS ', p.name
            gmess.add((reg_obj , PRODUCT.name,Literal(p.name) ) )

        print 'products added'
        agentToReceive = centroLogisticoAgent.address
        print 'Agent to receive is ', agentToReceive


        print 'We are goint to add the products '

        builtMsg = build_message(
            gmess,
            perf= ACL.request,
            sender = tiendaAgent.uri,
            receiver= centroLogisticoAgent.uri,
            content=reg_obj)

        print 'Message built'

        gr = send_message(
            builtMsg,
            agentToReceive
        )

        msgdic = get_message_properties(gr)
        content = msgdic['content']
        #print 'Content got - ', content

        # info to get the nearest centro logístico
        centroName = gr.value(subject= content, predicate= CL.name)
        centroLat = gr.value(subject= content, predicate= CL.lat)
        centroLong = gr.value(subject= content, predicate= CL.long)

        transName = gr.value(subject= content, predicate= TR.name )
        transEntrega = gr.value(subject= content, predicate= TR.dias_entrega)
        transTarifa = gr.value(subject= content, predicate= TR.tarifa)

        centroObject = {}
        centroObject['name'] =centroName
        centroObject['lat'] =centroLat
        centroObject['long'] =centroLong

        transObject = {}
        transObject['name'] =transName
        transObject['entrega'] =transEntrega
        transObject['tarifa'] =transTarifa

        resultObject = {}
        resultObject['centro'] = centroObject
        resultObject['transportista'] = transObject

        return resultObject


    except Exception , e:
        print e
        return ""

def delegateExternalSeller(user,prefs,products):
    ##Todo, stub que siempre devuelve lo mismo
    try:
        global mss_cnt
        print 'sup'
        #Create graph to send
        gmess = Graph()
        #We make bindings
        gmess.bind('client', CLIENT)
        gmess.bind('compra', COMPRA)

        reg_obj =  AGN[vendedorExternoAgent.name + '.getInfoSale']

        #info to get the nearest centro logístico
        gmess.add((reg_obj,  CLIENT.lat, Literal(user.lat)))
        gmess.add((reg_obj,  CLIENT.long, Literal(user.long)))
        gmess.add((reg_obj,  COMPRA.maxDaysToDeliver, Literal(prefs['days'])))

        for p in products:
            gmess.add((reg_obj , PRODUCT.name,Literal(p.name) ) )

        agentToReceive = vendedorExternoAgent.address

        builtMsg = build_message(
            gmess,
            perf= ACL.request,
            sender = tiendaAgent.uri,
            receiver= vendedorExternoAgent.uri,
            content=reg_obj)

        print 'Message built'

        gr = send_message(
            builtMsg,
            agentToReceive
        )

        msgdic = get_message_properties(gr)
        content = msgdic['content']

        transName = gr.value(subject= content, predicate= TR.name )
        transEntrega = gr.value(subject= content, predicate= TR.dias_entrega)
        transTarifa = gr.value(subject= content, predicate= TR.tarifa)

        centroObject = {}
        centroObject['name'] = 'Vendedor Externo'
        centroObject['lat'] = 40.434429
        centroObject['long'] = -3.713178

        transObject = {}
        transObject['name'] =transName
        transObject['entrega'] =transEntrega
        transObject['tarifa'] =transTarifa

        resultObject = {}
        resultObject['centro'] = centroObject
        resultObject['transportista'] = transObject

        return resultObject

    except Exception , e:
        print e
        return ""


    return

def insertSale(user, products, packageInfo):

    print "------------INSERT SALE------------"
    g = Graph()

    g.parse('compra/compra.rdf')

    print "inicializo el grafo"

    myid = uuid.uuid4()

    print myid

    misubject = "http://www.agentes.org/compra#%s" % myid
    misubject = URIRef(misubject)
    typeCompra = URIRef('http://www.agentes.org/compra#compra')
    listaProductos = URIRef('http://www.agentes.org/compra#listaProductos')
    userUri = URIRef('http://www.agentes.org/cliente#username')


    g.add((misubject, RDF.type, typeCompra))

    g.add((misubject, CLIENT.username, Literal(user.username) ))

    g.add((misubject, COMPRA.id_compra, Literal(myid) ))

    g.add((misubject, COMPRA.procedencia_envio, Literal(packageInfo["centro"]["name"]) ))
    g.add((misubject, COMPRA.lat_procedencia_envio, Literal(packageInfo["centro"]["lat"]) ))
    g.add((misubject, COMPRA.long_procedencia_envio, Literal(packageInfo["centro"]["long"]) ))

    g.add((misubject, TR.name, Literal(packageInfo["transportista"]["name"]) ))
    g.add((misubject, TR.dias_entrega, Literal(packageInfo["transportista"]["entrega"]) ))
    g.add((misubject, TR.tarifa, Literal(packageInfo["transportista"]["tarifa"]) ))


    print "despues de añadir el username"

    bag = BNode()

    g.add((misubject, listaProductos, bag))

    g.add((bag, RDF.type, RDF.Bag))

    i = 1
    print i
    for p in products:
        # productName = p.name
        print i
        productURI = p.a
        itemX = "http://www.w3.org/1999/02/22-rdf-syntax-ns#_%s" % i
        g.add((bag, URIRef(itemX), productURI))
        i = i + 1


    try:
        g.serialize("compra/compra.rdf",format="xml",encoding="utf-8")

    except Exception, e:
        print e

    printGraph(g)

    return myid

def createSale(user, prefs, products):

    print 'Hello printing sale'

    print '-------------------- user ------------------ '
    print user

    print "------ products -------"
    print products.result

    print '-------------------- prefs -------------------'

    username = user.username
    print '************************ username: %s ***********************' % (username)

    days = prefs['days']
    print '************************ days: %s ***********************' % (days)

    print 'Seller type: ', prefs['sellerType']

    # Miramos quien se encarga del envio y lo delegamos en él
    # El Ag. CentroLogístico cogerá el más cercano y enviará los productos
    # escogiendo el transportista más se ajuste a las preferencias


    if prefs['sellerType'] == "logisticCenter":
        print 'delegatiing'
        packageInfo = delegateCentroLogistico(user, prefs, products)
    else:
        packageInfo = delegateExternalSeller(user, prefs, products)

    print '------------------------- INFO PACKAGE -------------------------'
    print packageInfo['centro']['name']
    print '-----------------------------------------------------------------'
    print packageInfo['centro']['lat']
    print '-----------------------------------------------------------------'
    print packageInfo['centro']['long']
    print '-----------------------------------------------------------------'
    print packageInfo['transportista']['name']
    print '-----------------------------------------------------------------'
    print packageInfo['transportista']['entrega']
    print '-----------------------------------------------------------------'
    print packageInfo['transportista']['tarifa']
    print '------------------------- INFO PACKAGE -------------------------'

    compraId = insertSale(user, products, packageInfo)

    packageInfo['saleId'] = compraId
    return packageInfo


def createProductFromExternalSeller(prefs):
    productGraph = Graph()

    productGraph.parse('producto/product.rdf');

    productGraph.bind('RDF', RDF)
    productGraph.bind('FOAF', FOAF)
    productGraph.bind('PRODUCT', PRODUCT)

    producto = PRODUCT[prefs['name']]

    name = Literal(prefs['name'])
    productType = Literal(prefs['productType'])
    price = Literal(prefs['price'])
    vendedor = Literal(prefs['sellerType'])

    productGraph.add((producto, RDF.type, PRODUCT.Product))
    productGraph.add((producto, PRODUCT.name, name))
    productGraph.add((producto, PRODUCT.type, productType))
    productGraph.add((producto, PRODUCT.precio, price))
    productGraph.add((producto, PRODUCT.vendedor, vendedor))

    # Iterate over triples in store and print them out.
    #print("--- printing raw triples ---")
    #for s, p, o in productGraph:
    #    print((s, p, o))

    # print producto
    productGraph.serialize("producto/product.rdf",format="xml",encoding="utf-8")

    return


'''
----------------------------- FLASK ROUTES -----------------------------------
'''


@app.errorhandler(500)
def exception_handler(error):
    return render_template('errorPage.html',code=500,title="Something happened :(")

@app.errorhandler(404)
def exception_handler(error):
    return render_template('errorPage.html',code=400,text="Oooops")


@app.route("/")
def render_main():
    """
        Main page for tiendaAgent
    """
    return render_template('formBusqueda.html', title='TIENDA')
    #return render_template('tienda.html', title='TIENDA')

@app.route("/search")
def render_search():
    """
        Main page for tiendaAgent
    """
    return render_template('formBusqueda.html', title='TIENDA')

@app.route("/refund" , methods=['GET'])
def refund_main():
    return render_template('formDevolucio.html', title='Devolucion producto')



@app.route("/newProduct" , methods=['GET'])
def add_main():
    return render_template('altaProducto.html', title='Alta producto')



@app.route("/newProduct", methods=['POST'])
def form_add_product():
    print 'Formulario recibido newProduct'
    prefs = {}

    prefs['name'] = getForm('productName')


    if not prefs['name']:
        return printErrorPage("Provide a name", 400)

    prefs['productType'] = getForm('productType')

    prefs['price'] = getForm('price')

    if not prefs['price']:
        return printErrorPage("Provide a price", 400)

    prefs['sellerType'] = getForm('sellerType')

    createProductFromExternalSeller(prefs)

    return render_template('formSubmittedOk.html',message="Producto creado correctamente")

@app.route("/Stop")
def stop():
    """
    Entrypoint que para el agente

    :return:
    """
    tidyup()
    shutdown_server()
    return "Parando Servidor"




@app.route("/refund",methods=['POST'])
def form_refund():
    print 'Formulario recibido refund'
    prefs = {}

    saleId  = getForm('saleId')

    if not saleId:
        return printErrorPage("Provide a sale Id", 400)

    productName = getForm('productName')

    if not productName:
        return printErrorPage("You must provide a list of products", 400)

    g = Graph()
    g.parse('compra/compra.rdf')
    #printGraph(g)
    url = "http://www.agentes.org/compra#%s" % saleId
    bob = URIRef(url)


    futuroSub = None


    for s,p,o in g.triples((bob,None,None)):
        if s is None:
            return printErrorPage("The sale doesn't exists", 400)

        futuroSub = s
        #g.add((s, RDF.type, None))
        for p, o in g.predicate_objects(subject=s):
            print '------------------------'
            print p
            print o

    if futuroSub is None:
        return printErrorPage("El id de la compra no es correcto",400)


    b = esPosibleDevolver()
    if not b:
        return printErrorPage("Devolucion no concedida",400)


    #listaProductos = URIRef('http://www.agentes.org/compra#dev')
    #bib = URIRef("http://www.agentes.org/compra#dev_%s" % saleId)
    print "intentando añadir la devolucion"
    typeDev = URIRef('http://www.agentes.org/compra#devolucion')
    listaProductosDev = URIRef('http://www.agentes.org/compra#listaProductosDevueltos')

    g.add((futuroSub, RDF.type, typeDev))

    words = productName.split()
    print "-- NAMES --"

    bag = BNode()

    g.add((futuroSub, listaProductosDev, bag))

    g.add((bag, RDF.type, RDF.Bag))

    # for each word in the line:
    i = 0
    for word in words:
        i = i + 1
        print word
        # print the word
        productURI = "http://www.agentes.org/product#%s" % word
        productURI = URIRef(productURI)
        itemX = "http://www.w3.org/1999/02/22-rdf-syntax-ns#_%s" % i
        g.add((bag, URIRef(itemX), productURI))
        print(productURI)

    try:
        g.serialize("compra/compra.rdf",format="xml",encoding="utf-8")
    except Exception, e:
        print 'ERROOOOOOOOOOOOOOOR', e

    print g.serialize()

    print 'saleId', saleId
    print 'productName', productName

    return render_template('formSubmittedOk.html', message="Tu producto ha sido devuelto correctamente" )

@app.route("/search",methods=['POST'])
def form_search():

    print 'Formulario recibido search'
    prefs = {}

    username = getForm('username')

    if not username:
        return printErrorPage("Provide an username", 400)

    prefs['name'] = getForm('productName')
    prefs['days'] = getForm('days')
    prefs['productType'] = getForm('productType')
    prefs['minPrice'] = getForm('minPrice')
    prefs['maxPrice'] = getForm('maxPrice')
    prefs['sellerType'] = getForm('sellerType')

    printPrefs(prefs)

    user = findUser(username)
    if not user:
        return printErrorPage("User doesn't exists", 404)

    products = findProducts(prefs)
    print 'PRODUCTS ', products
    print 'RESULT ', products.result

    if not products.result:
        return printErrorPage("No products were found with your criteria",404)


    info = createSale(user, prefs, products)

    print 'AFTER CREATE SALE ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
    subtotal = 0
    for p in products:
        print 'PRECIO PRODUCT ', p.precio
        try:
            dec = Decimal(p.precio)
        except Exception, e:
            print 'ERROR ', e
        print 'DECIMAAAL ', dec
        subtotal += dec

    print 'user is ', user

    total = subtotal + Decimal(info['transportista']['tarifa'])

    return render_template('compra.html',products=products, info=info, username=user.name.replace("_"," "), subtotal=subtotal, total=total)


'''
----------------------------- CSS AND JS -----------------------------------
'''
@app.route('/templates/js/<path:path>')
def send_js(path):
	return send_from_directory('templates/js',path)

@app.route('/templates/css/<path:path>')
def send_styles(path):
	return send_from_directory('templates/css',path)

@app.route('/templates/fonts/<path:path>')
def send_icons(path):
	return send_from_directory('templates/fonts',path)



'''
----------------------------- FLASK LOGIC -----------------------------------
'''

def tidyup():
    """
    Acciones previas a parar el agente

    """
    return


def agentbehavior1(cola):
    """
    Un comportamiento del agente

    :return:
    """
    return



'''
Función para obtener las request del Flask y evitar Exceptions
'''
def getForm(name):
    try:
        result = request.values[name]
    except Exception, e:
        result = ""
    return result


def getFormList(name):
    result = []
    try:
        result = request.values.getlist(name)
    except Exception, e:
        result = ""
    return result


if __name__ == '__main__':
    print 'Start tienda agent'

    # Ponemos en marcha los behaviors
    ab1 = Process(target=agentbehavior1, args=(cola1,))
    ab1.start()

    # Ponemos en marcha el servidor
    app.run(host=hostname, port=port)

    # Esperamos a que acaben los behaviors
    ab1.join()

    print 'The End'
